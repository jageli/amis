import uvicorn
from fastapi import FastAPI, applications, Request
from fastapi.openapi.docs import get_swagger_ui_html
from starlette.staticfiles import StaticFiles
from starlette.templating import Jinja2Templates

app = FastAPI(
    title="FastAPI Admin",
    description="",
    version="v1",
    docs_url="/docs",
    redoc_url="/redocs",
)

app.mount(path="/static", app=StaticFiles(directory="./static"), name="static")
templates = Jinja2Templates(directory="./static/templates")


def swagger_monkey_patch(*args, **kwargs):
    """
    Wrap the function which is generating the HTML for the /docs endpoint and
    overwrite the default values for the swagger js and css.
    """
    return get_swagger_ui_html(
        *args,
        **kwargs,
        # swagger_js_url="https://cdn.jsdelivr.net/npm/swagger-ui-dist@3.29/swagger-ui-bundle.js",
        # swagger_css_url="https://cdn.jsdelivr.net/npm/swagger-ui-dist@3.29/swagger-ui.css")
        swagger_js_url="static/js/swagger-ui-bundle.js?v=1",
        swagger_css_url="static/css/swagger-ui.css?v=1")


applications.get_swagger_ui_html = swagger_monkey_patch


@app.get("/", summary='mysite')
async def main(request: Request):

    return templates.TemplateResponse("hello2.html", {"request": request})


@app.get("/test", summary='mysite')
async def main():
    data = {
        "status": 0,
        "msg": "",
        "data": {
            "title": "Test Page Component",
            "date": "2017-10-131"
        }
    }
    return data


if __name__ == "__main__":
    uvicorn.run("API:app",
                host="127.0.0.2",
                port=8000,
                reload=True,
                debug=True,
                workers=1)
